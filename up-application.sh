#!/bin/bash

nvm use v10.5.0

docker start sbcontabil_db

pm2 start ecosystem.config.js --env production
