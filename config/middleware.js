module.exports = function(req, res, next) {
    var path = req.path.split('/');
    var routesIgnore = ['sig-in','profile-create','create-perfil','website'];
    let verifyRoute = false;
    routesIgnore.forEach(element => {
        if(path[1] === element){
            verifyRoute = true;
        }
    });

    if(req.session.login || verifyRoute){
        next()
    }else{
        res.render('admin/form_login',{login : false})
    }
}