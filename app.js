var app = require('./config/server');
var middleware = require('./config/middleware');
app.use(middleware)
const { PORT_PROXY } = process.env;
app.listen(PORT_PROXY, function() {
	console.log(`PORT ${PORT_PROXY}`)
	console.log('Servidor ON')
})
