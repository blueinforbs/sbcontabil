# SContabil
Sistema financeiro particular

#pré requisitos:

- NodeJS versão 10.5.0
- Docker
- Docker compose

#Instalação:

- rodar comando npm install

#configurar banco de dados com Docker:

- Entrar no diretório .docker/nodejs e executar comando docker build -t nodejs-rodrigo:10.5 .
- executar na raiz docker-compose up -d

#config mysql8:

- Executar comando ./install.sh