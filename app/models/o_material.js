module.exports = function () {
    this.list = (connection, callback) => {
        connection.query("SELECT * FROM o_material  ", callback)
    }

    this.store = (post, connection, callback) => {
        connection.query("INSERT INTO o_material SET ?", post, callback)
    }

    return this
}
