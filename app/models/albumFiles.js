module.exports = function () {
    this.selectAll = (idUser, connection, callback) => {
        connection.query("SELECT * FROM album_files WHERE id_user = "+idUser+" ORDER BY id DESC", callback)
    }

    this.get = (idalbum_files, connection, callback) => {
        connection.query("SELECT * FROM album_files WHERE id = "+idalbum_files+" ", callback)
    }

    this.store = (post, connection, callback) => {
        connection.query("INSERT INTO album_files SET ?", post, callback)
    }

    this.delete = (id, connection, callback) => {
        connection.query("DELETE FROM album_files WHERE id = "+id,null,callback)
    }

    this.albumFiles = (idUser,connection, callback) => {
        connection.query("SELECT af.id ,f.path_file, a.label FROM album_files as af  "+
        "INNER JOIN album a ON(a.id=af.id_album)"+
        "  INNER JOIN files f ON(f.id=af.id_files)"+
        " WHERE f.id_user = "+idUser +" AND a.id_user = "+idUser
        , callback)
    }

    return this
}
