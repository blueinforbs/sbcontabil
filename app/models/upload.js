var formidable = require('formidable')
var fs = require('fs')
const files = require('./files')

module.exports = () => {
    this.upload = (req, res, server, callback) => {
        var form = new formidable.IncomingForm()
        var connection = server.config.dbConnection();
        var idUser = req.session.login.id

        form.parse(req, (err, fields, files) => {
            var albumId = fields.album;
            var albumModel = server.app.models.album;
            let oldpath = files.file.filepath;
            let filename = files.file.originalFilename;
            albumModel.get(idUser, albumId, connection, function (erro, result) {
                
                let newpath = 'public/galeria/' + result[0].name + "/" + files.file.originalFilename;
                let pathSave = result[0].name + "/" + files.file.originalFilename;
                fs.copyFile(oldpath, newpath, function (err) {
                    if (err) throw err;
                    callback({
                        erro: 'false',
                        message: 'File uploaded and moved!',
                        path: newpath,
                        albumId: albumId
                    })
                    filesModel = server.app.models.files;
                    post = {
                        name: filename,
                        id_user: idUser,
                        path_file: pathSave
                    }
                    filesModel.store(post, connection, function (erro, result){
                        albumFilesPost = {
                            id_files : result.insertId,
                            id_album : albumId
                        }
                        albumFilesModels = server.app.models.albumFiles;
                        albumFilesModels.store(albumFilesPost, connection);
                    })
                    res.end();
                });
            })
        })
        return callback
    }

    this.createAlbum = (req, res, callback) => {
        var folder = 'public/galeria/' + req.body.folder
        try {
            if (!fs.existsSync(folder)) {
                fs.mkdirSync(folder)
                // criar registro
                callback({ erro: 'false', message: 'Diretório criado!', value: true })
            } else {
                callback({ erro: 'false', message: 'Diretório já existe!', value: false })
            }
        } catch (err) {
            console.log(err)
        }
        // res.end()
        return callback
    }

    return this;
}