module.exports = function () {
    this.albumFiles = (connection, callback) => {
        connection.query("SELECT a.name ,a.label ,f.path_file, a.id as idAlbum, f.id FROM album_files af  "+
        "INNER JOIN album a ON(a.id=af.id_album)"+
        "  INNER JOIN files f ON(f.id=af.id_files)"
        , callback)
    }

    return this
}
