module.exports = function () {
    this.selectAll = (idUser, connection, callback) => {
        connection.query("SELECT * FROM files WHERE id_user = "+idUser+" ORDER BY id DESC", callback)
    }

    this.get = (idUser, idfiles, connection, callback) => {
        connection.query("SELECT * FROM files WHERE id_user = "+idUser+" AND id = "+idfiles+" ", callback)
    }

    this.store = (post, connection, callback) => {
        connection.query("INSERT INTO files SET ?", post, callback)
    }

    this.delete = (id, connection, callback) => {
        connection.query("DELETE FROM files WHERE id = "+id,null,callback)
    }

    return this
}
