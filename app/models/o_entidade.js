module.exports = function () {
    this.list = (connection, callback) => {
        connection.query("SELECT * FROM o_entidade  ", callback)
    }

    this.store = (post, connection, callback) => {
        connection.query("INSERT INTO o_entidade SET ?", post, callback)
    }

    return this
}
