module.exports = function () {
    this.list = (connection, callback) => {
        connection.query("SELECT ome.id,ome.valor,m.description as material,e.description as entidade FROM o_material_entidade_valor ome "+
        " LEFT JOIN o_material m ON(m.id=ome.id_o_material)"+
        " LEFT JOIN o_entidade e ON(e.id=ome.id_o_entidade)", callback)
    }

    this.store = (post, connection, callback) => {
        connection.query("INSERT INTO o_material_entidade_valor SET ?", post, callback)
    }

    return this
}
