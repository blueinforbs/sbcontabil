module.exports = function () {
    this.selectAll = (idUser, connection, callback) => {
        connection.query("SELECT * FROM album WHERE id_user = "+idUser+" ORDER BY id DESC", callback)
    }

    this.get = (idUser, idAlbum, connection, callback) => {
        connection.query("SELECT * FROM album WHERE id_user = "+idUser+" AND id = "+idAlbum+" ", callback)
    }

    this.store = (post, connection, callback) => {
        connection.query("INSERT INTO album SET ?", post, callback)
    }

    this.delete = (id, connection, callback) => {
        connection.query("DELETE FROM album WHERE id = "+id,null,callback)
    }

    return this
}
