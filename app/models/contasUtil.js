module.exports = function () {
    this.getTotalMes = (limite, mes, ano, connection, callback) => {
        var qLimite = ''
        if(limite){
            qLimite = '- (SELECT utilizado FROM limite_conta)'
        }
        connection.query("SELECT SUM(valorparcela) as total,"
            + "((SELECT valor FROM salario WHERE MONTH(data) = " + mes + " AND YEAR(data) = " + ano + ")-SUM(valorparcela)) AS saldo,"
            + "((SELECT valor FROM salario WHERE MONTH(data) = " + mes + " AND YEAR(data) = " + ano + ") "+qLimite+") - SUM(valorparcela) AS ssaldo,"
            + " MONTH(vencimento) as mes, "
            + "((SELECT valor FROM salario WHERE MONTH(data) = " + mes + " AND YEAR(data) = " + ano + ")-SUM(valorparcela)) AS saldo,"
            + "(SELECT valor FROM salario WHERE MONTH(data) = " + mes + " AND YEAR(data) = " + ano + ") AS salario "
            + " FROM contas_pagar WHERE MONTH(vencimento) = " + mes + " and YEAR(vencimento) = " + ano + " GROUP BY MONTH(vencimento);", callback)
    }

    this.getTotalMesRest = (connection, callback) => {
        connection.query("SELECT MONTH(vencimento) as mes,"
            + "((SELECT valor FROM salario s WHERE YEAR(s.data) = YEAR(now()) ORDER BY idsalario DESC LIMIT 1) - SUM(valorparcela)) as total "
            + "FROM contas_pagar cp  "
            + "WHERE YEAR(vencimento) = YEAR(now()) GROUP BY MONTH(vencimento)"
            , callback)
    }

    return this;
}