module.exports.contas = async (server, req, res) => {
    var db = server.config.dbConnection();
    var modelTeste = server.app.models.teste;
    var h = await new Promise((resolve) => {
        modelTeste.get(db, (err, result) => {
            resolve(result)
        })
    })
    res.send(h)
}

module.exports.contasReceberDelete = async (server, req, res) => {
    var db = server.config.dbConnection();
    var modelContas = server.app.models.contas;
    var id = req.params.id;

    await modelContas.delContasReceber(id, db, (err, result) => {
        if (err) {
            console.log(err)
        } else {
            modelContas.delConta(id, db, (err, result) => {
                if (err) {
                    console.log(err)
                }
            });
        }
    });
    res.redirect("/contas-receber");
}

module.exports.bkp = (server, req, res) => {
    const data = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    res.send(data)

}