module.exports.initPage = (server, req, res) => {
    var connection = server.config.dbConnection();
    var homeModel = server.app.models.home;
    homeModel.albumFiles(connection, function (
        erro,
        result
    ) {
        if (erro) {
            console.log(erro)
        }
        var galeria = [];
        var category = [];
        result.forEach(element => {
            category[element.idAlbum] = element.label;
            if (galeria[element.idAlbum]) {
                galeria[element.idAlbum].push(element)
            } else {
                galeria[element.idAlbum] = []
                galeria[element.idAlbum].push(element)
            }
        });

        res.render('site/home', { login: false, galeria: galeria, category: category, website: true })
    });


}
