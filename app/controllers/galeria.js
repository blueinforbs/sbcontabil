module.exports.indexImportar = (server, req, res) => {
    var connection = server.config.dbConnection();
    var albumFilesModel = server.app.models.albumFiles;
    var albumModel = server.app.models.album;
    var idUser = req.session.login.id;
    albumFilesModel.albumFiles(idUser, connection, function (
        erro,
        result
    ) {
        if (erro) {
            console.log(erro)
        }
        let albumFiles = result;

        albumModel.selectAll(idUser, connection, function (erro2, result2 ){

            let album = result2;
            res.render('galeria/importar-index', { message: false, dados: albumFiles, albuns: album })
        })

        
    });

}

module.exports.deleteFile = (server,req,res) => {
    var connection = server.config.dbConnection();
    var albumFilesModel = server.app.models.albumFiles;
    var filesModel = server.app.models.files;
    albumFilesModel.get(req.params.id, connection, function (
        erro,
        result
    ) {
        if (erro) {
            console.log(erro)
        }
        var idFile = result[0].id_files
        albumFilesModel.delete(req.params.id,connection)
        filesModel.delete(idFile,connection)
    });
    res.redirect('/galeria/importar')
}

module.exports.storeImportar = (server, req, res) => {
    server.app.models.upload.upload(req, res, server, function (dados) {
        res.redirect('/galeria/importar')
    })
}

module.exports.indexAlbum = (server, req, res) => {
    var connection = server.config.dbConnection();
    var albumModel = server.app.models.album;
    var idUser = req.session.login.id;
    albumModel.selectAll(idUser, connection, function (
        erro,
        result
    ) {
        if (erro) {
            console.log(erro)
        }
        res.render('galeria/album-index', { message: false, dados: result })
    });

}

module.exports.storeAlbum = async (server, req, res) => {
    let list = ''
    var connection = server.config.dbConnection();
    var albumModel = server.app.models.album;
    var idUser = req.session.login.id

    await server.app.models.upload.createAlbum(req, res, function (dados) {
        var label = req.body.titulo
        var post = {
            name: req.body.folder,
            id_user: idUser,
            label: label
        }

        if(dados.value){
            albumModel.store(post, connection);
        }
        list = dados
    })

    await albumModel.selectAll(idUser, connection, function (
        erro2,
        result2
    ) {
        if (erro2) {
            console.log(erro2)
        }
        res.render('galeria/album-index', { message: list.message, dados: result2})
    });

}
