module.exports.listMaterial = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_materialModel = server.app.models.o_material;

    let list = await new Promise (async(resolve) => {
        await o_materialModel.list(connection,function (err, result) {
            resolve(result)
        })
    })

    res.render('orcamento/material/index', { message: null, dados: list })
}

module.exports.saveMaterial = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_materialModel = server.app.models.o_material;

    var post = {
        description: req.body.description
    }

    await new Promise (async(resolve) => {
        await o_materialModel.store(post,connection,function (err, result) {
            resolve(result)
        })
    })

    res.redirect('material')
}

module.exports.listEntidade = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_endtidadeModel = server.app.models.o_entidade;

    let list = await new Promise (async(resolve) => {
        await o_endtidadeModel.list(connection,function (err, result) {
            resolve(result)
        })
    })

    res.render('orcamento/entidade/index', { message: null, dados: list })
}

module.exports.saveEntidade = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_entidadeModel = server.app.models.o_entidade;

    var post = {
        description: req.body.description
    }

    await new Promise (async(resolve) => {
        await o_entidadeModel.store(post,connection,function (err, result) {
            resolve(result)
        })
    })

    res.redirect('estabelecimento')
}

module.exports.listOrcamento = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_orcamentoModel = server.app.models.o_orcamento;

    let list = await new Promise (async(resolve) => {
        await o_orcamentoModel.list(connection,function (err, result) {
            resolve(result)
        })
    })

    res.render('orcamento/orcamento/index', { message: null, dados: list })
}

module.exports.saveOrcamento = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_orcamentoModel = server.app.models.o_orcamento;
    
    var valor = req.body.valor.replace(".", "");
    valor = valor.replace(",", ".");

    var post = {
        id_o_entidade: req.body.id_entidade,
        id_o_material: req.body.id_material,
        valor: valor
    }

    await new Promise (async(resolve) => {
        await o_orcamentoModel.store(post,connection,function (err, result) {
            console.log(err)
            resolve(result)
        })
    })

    res.redirect('orcamento')
}

module.exports.newOrcamento = async (server, req, res) => {
    var connection = server.config.dbConnection();
    var o_materialModel = server.app.models.o_material;
    var o_entidadeModel = server.app.models.o_entidade;

    let material = await new Promise (async(resolve) => {
        await o_materialModel.list(connection,function (err, result) {
            resolve(result)
        })
    })

    let entidade = await new Promise (async(resolve) => {
        await o_entidadeModel.list(connection,function (err, result) {
            resolve(result)
        })
    })

    res.render('orcamento/orcamento/form_add', { message: null, material: material, entidade: entidade })
}


