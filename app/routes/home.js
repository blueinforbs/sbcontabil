module.exports = function (app) {
    app.get('/', async function (req, res) {
        var contasModel = app.app.models.contas
        var connection = app.config.dbConnection();
        var user = (req.session.login) ? req.session.login : null

        var final = await new Promise((resolve, reject) => {
            contasModel.getContaTotalResto('2024', connection, async (erro, response) => {
                resolve(response);
            });
        });
        var pagar2022 = await new Promise((resolve, reject) => {
            contasModel.getContaTotalResto('2024', connection, async (erro, pagar) => {
                resolve(pagar);
            });
        })

        var totais = await new Promise((resolve, reject) => {
            contasModel.getContaTotalAno(connection, (erro, response) => {
                resolve(response)
            });
        });
        res.render("home/index", { totais: totais, resto: final, resto2022: pagar2022, usuario: user })
    })
}