module.exports = function (app) {



  app.get("/contas-anual", async function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    await contasModel.getContaTotalAno(connection, (erro, response) => {
      res.send({ totais: response });
    });
  });

  app.post("/contas_pagar/atualizar/:id", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    var post = req.body;
    post.id = req.params.id;
    post.status = "S";
    post.valorpago = post.valorpago.replace(".", "");
    post.valorpago = post.valorpago.replace(",", ".");

    contasModel.updateContaPagar(post, connection, function (erro, result) {
      if (erro) {
        console.log(erro);
        res.render("admin/form_update_conta_pagar", {
          message: "Erro ao inserir dados",
          id: post.id,
        });
      } else {
        contasModel.getContasPagar(post.id, connection, function (
          erro,
          result
        ) {
          res.redirect("/contas/" + result[0].idcontas + "/pagar");
        });
      }
    });
  });

  app.post("/contas_pagar/atualizar/v/:id", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    var post = req.body;
    post.id = req.params.id;
    post.status = "N";

    if (post.valor) {
      post.valor = post.valor.replace(".", "");
      post.valor = post.valor.replace(",", ".");
    }

    contasModel.updateContaVencimento(post, connection, function (
      erro,
      result
    ) {
      if (erro) {
        console.log(erro);
        res.render("admin/form_update_vencimento_conta_pagar", {
          message: "Erro ao inserir dados",
          id: post.id,
        });
      } else {
        contasModel.getContasPagar(post.id, connection, function (
          erro,
          result
        ) {
          res.redirect("/contas/" + result[0].idcontas + "/pagar");
        });
      }
    });
  });

  app.post("/contas/gravar", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    var post = req.body;
    post.valor = post.valor.replace(".", "");
    post.valor = post.valor.replace(",", ".");

    post.usuario = req.session.login.id
    contasModel.insertConta(post, connection, function (erro, result) {
      var id = result.insertId;
      var parc = req.body.qtd_parcelas;

      if (req.body.tipo == "pagar") {
        dados = {
          nome: post.conta,
          valorparcela: post.valor,
          status: "N",
          idcontas: id,
          flag: req.body.flag,
        };
        for (var i = 1; i <= parc; i++) {
          dados.numeroparcela = i;
          contasModel.insertContasPagar(dados, connection, function (
            erro,
            result
          ) { });
        }
      } else {
        dados = {
          nome: post.conta,
          valorparcela: post.valor,
          status: "N",
          idcontas: id,
        };
        for (var i = 1; i <= parc; i++) {
          dados.numeroparcela = i;
          contasModel.insertContasReceber(dados, connection, function (
            erro,
            result
          ) { });
        }
      }
      if (erro) {
        console.log(erro);
        res.render("admin/form_add_conta", {
          message: "Erro ao inserir dados",
        });
      } else {
        res.render("admin/form_add_conta", {
          message: "Dados inseridos com sucesso",
        });
      }
    });
  });

  app.get("/contas", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    contasModel.getContas(connection, function (erro, result) {
      res.render("contas/contas", { contas: result, title: "a pagar", type: 'pagar' });
    });
  });

  app.get("/contas-receber", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    contasModel.getReceber(connection, function (erro, result) {
      res.render("contas/contas", { contas: result, title: "a receber", type: 'receber' });
    });
  });
  // INICIO CONTAS RECEBER

  app.get("/contas-receber/:id/delete", function (req, res) {
    app.app.controllers.contas.contasReceberDelete(app, req, res);
  });

  app.post("/contas_receber/atualizar/:id", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    var post = req.body;
    post.id = req.params.id;
    post.status = "S";
    post.valorpago = post.valorpago.replace(".", "");
    post.valorpago = post.valorpago.replace(",", ".");

    contasModel.updateContaReceber(post, connection, function (erro, result) {
      if (erro) {
        console.log(erro);
        res.render("admin/form_update_conta_receber", {
          message: "Erro ao inserir dados",
          id: post.id,
        });
      } else {
        contasModel.getContasReceber(post.id, connection, function (
          erro,
          result
        ) {
          res.redirect("/contas/" + result[0].idcontas + "/receber");
        });
      }
    });
  });

  app.post("/contas_receber/atualizar/v/:id", function (req, res) {
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    var post = req.body;
    post.id = req.params.id;
    post.status = "N";

    if (post.valor) {
      post.valor = post.valor.replace(".", "");
      post.valor = post.valor.replace(",", ".");
    }

    contasModel.updateContaReceberVencimento(post, connection, function (
      erro,
      result
    ) {
      if (erro) {
        console.log(erro);
        res.render("admin/form_update_vencimento_conta_receber", {
          message: "Erro ao inserir dados",
          id: post.id,
        });
      } else {
        contasModel.getContasReceber(post.id, connection, function (
          erro,
          result
        ) {
          res.redirect("/contas/" + result[0].idcontas + "/receber");
        });
      }
    });
  });

  // FIM CONTAS RECEBER

  app.get("/contas/:id/:tipo", function (req, res) {
    var params = req.params;
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    var format = require("date-format");
    var numeral = require("numeral");

    if (params.tipo == "pagar") {
      contasModel.getContaPagar(params.id, connection, function (erro, result) {
        res.render("contas/contas_pagar", {
          contas: result,
          format: format,
          numeral: numeral,
        });
      });
    } else if (params.tipo == "receber") {
      contasModel.getContaReceber(params.id, connection, function (
        erro,
        result
      ) {
        res.render("contas/contas_receber", {
          contas: result,
          format: format,
          numeral: numeral,
        });
      });
    }
  });

  app.get("/payment-all-month/:m/:y", function (req, res) {
    var params = req.params;
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();

    contasModel.updateContasPagarPaymentAllMonth(params, connection, function (
      erro,
      result
    ) {
      if (erro) {
        res.status(500).send(erro);
      } else {
        res.status(200).send("atualizado");
      }
    });
    return;
  });

  app.get("/payment-all-month-revert/:m/:y", function (req, res) {
    var params = req.params;
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();

    contasModel.updateContasPagarPaymentAllMonthRevert(
      params,
      connection,
      function (erro, result) {
        if (erro) {
          console.log(erro);
          res.status(500).send(erro);
        } else {
          res.status(200).send("atualizado");
        }
      }
    );
    return;
  });

  app.get("/rest_per_month", function (req, res) {

    var contasUtilsModel = app.app.models.contasUtil;
    var connection = app.config.dbConnection();


    contasUtilsModel.getTotalMesRest(connection, function (erro, result) {
      if (erro) {
        res.send(erro);
      } else {
        res.send(result);
      }
    });

  })

  app.get("/monthly_statistics", function (req, res) {

    var params = req.query;
    var contasUtilsModel = app.app.models.contasUtil;
    var connection = app.config.dbConnection();

    var data = new Date();
    var mes = (data.getUTCMonth() + 2);
    var ano = data.getFullYear();
    var limite = false
    if (params.mes) {
      mes = params.mes
    }

    if (params.ano) {
      ano = params.ano
    }

    if (params.limite) {
      limite = params.limite
    }

    contasUtilsModel.getTotalMes(limite, mes, ano, connection, function (erro, result) {
      if (erro) {
        res.send(erro);
      } else {
        let dados = {
          total: result[0].total,
          saldo: result[0].saldo,
          ssaldo: result[0].ssaldo,
          mes: result[0].mes,
          salario: result[0].salario
        }
        res.send(dados);
      }
    });

  });

  app.route("/banco")
    .get(function (req, res) {
      var bancoModel = app.app.models.banco;
      var connection = app.config.dbConnection();
      bancoModel.getLimiteLast(connection, function (erro, result) {
        res.render("contas/banco_index", { limite: result });
      });
    })
    .post(function (req, res) {
      var post = req.body;
      post.total = post.total.replace(".", "");
      post.total = post.total.replace(",", ".");

      post.utilizado = post.utilizado.replace(".", "");
      post.utilizado = post.utilizado.replace(",", ".");

      var bancoModel = app.app.models.banco;
      var connection = app.config.dbConnection();
      // bancoModel.getLimiteLast(connection, function (erro, result) {
      //   res.render("contas/banco_index",{ limite : result});
      // });

      bancoModel.updateBanco(post, connection, function (erro, result) {
        if (erro) {
          console.log(erro);
        } else {
          bancoModel.getLimiteLast(connection, function (erro, result) {
            res.render("contas/banco_index", { limite: result });
          });
        }
      });
    });

  app.get("/salario", function (req, res) {
    var salarioModel = app.app.models.salario;
    var connection = app.config.dbConnection();
    salarioModel.get(connection, function (erro, result) {
      res.render("contas/salario_index", { salario: result });
    });
  });

  app.get("/form/salario", function (req, res) {
    res.render("admin/form_add_salario");
  });

  app.post("/salario", function (req, res) {
    var post = req.body;
    post.valor = post.valor.replace(".", "");
    post.valor = post.valor.replace(",", ".");
    post.user_id = req.session.login.id;

    var salarioModel = app.app.models.salario;
    var connection = app.config.dbConnection();
    salarioModel.insert(post, connection, function (erro, result) {
      if (erro) {
        res.send(erro)
      } else {
        res.redirect("salario");
      }
    });
  });

  app.post("/salario-update", function (req, res) {
    var salarioModel = app.app.models.salario;
    var connection = app.config.dbConnection();
    var put = req.body;
    var length = put.dataTable_length;

    for (let i = 0; i < length; i++) {
      put.valor[i] = put.valor[i].replace(".", "");
      put.valor[i] = put.valor[i].replace(",", ".");
      var data = {
        valor: put.valor[i],
        id: put.id[i],
        user_id: req.session.login.id
      }
      salarioModel.update(data, connection, function (erro, result) {
      });
    }

    res.redirect("salario");

  });

  app.get("/reorganiza/:id", function (req, res) {
    var params = req.params
    var contasModel = app.app.models.contas;
    var connection = app.config.dbConnection();
    contasModel.getContasReorganiza(params.id, connection, function (erro, result) {
      res.send(result);
    });
  });

};
