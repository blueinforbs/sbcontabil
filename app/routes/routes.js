module.exports = (server) => {
  server.get("/teste", (req, res, next) => {
    server.app.controllers.contas.contas(server, req, res);
  });

  server.get("/bkp", (req, res, next) => {
    server.app.controllers.contas.bkp(server, req, res);
  });

  server.get("/captcha", (req, res, next) => {
    res.render('home/captcha');
  });

  server.get("/website", (req, res, next) => {
    server.app.controllers.website.initPage(server, req, res)
  });

  server.get('/galeria/importar', (req, res, next) => {
    server.app.controllers.galeria.indexImportar(server, req, res)
  });

  server.post('/galeria/importar', (req, res) => {
    server.app.controllers.galeria.storeImportar(server, req, res)
  });

  server.get('/galeria/album', (req, res, next) => {
    server.app.controllers.galeria.indexAlbum(server, req, res)
  });

  server.post('/galeria/album', (req, res, next) => {
    server.app.controllers.galeria.storeAlbum(server, req, res)
  });

  server.get('/galeria/files/:id/delete', (req, res) => {
    server.app.controllers.galeria.deleteFile(server, req, res)
  })

};
