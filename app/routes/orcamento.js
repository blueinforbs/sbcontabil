module.exports = (server) => {
    server.get('/material', (req, res, next) => {
        server.app.controllers.orcamento.listMaterial(server, req, res)
    });

    server.post('/material', (req, res, next) => {
        server.app.controllers.orcamento.saveMaterial(server, req, res)
    });

    server.get('/material/cad', (req, res, next) => {
        res.render('orcamento/material/form_add', { message: null })
    });

    /**
     * Entidade
     */
    server.get('/estabelecimento', (req, res, next) => {
        server.app.controllers.orcamento.listEntidade(server, req, res)
    });

    server.post('/estabelecimento', (req, res, next) => {
        server.app.controllers.orcamento.saveEntidade(server, req, res)
    });

    server.get('/estabelecimento/cad', (req, res, next) => {
        res.render('orcamento/entidade/form_add', { message: null })
    });

    /**
     * Orçamento
     */
    server.get('/orcamento', (req, res, next) => {
        server.app.controllers.orcamento.listOrcamento(server, req, res)
    });

    server.post('/orcamento', (req, res, next) => {
        server.app.controllers.orcamento.saveOrcamento(server, req, res)
    });

    server.get('/orcamento/cad', (req, res, next) => {
        server.app.controllers.orcamento.newOrcamento(server, req, res)
    });

};


