const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
module.exports = function (app) {
    app.post('/sig-in', async function (req, res) {
        var model = app.app.models.user;
        var dbConnection = app.config.dbConnection();

        model.get(req.body, dbConnection, async (err, result) => {
            if (err) {
                console.log(err)
                res.render("admin/form_login", { msg: 'Falha no login, tente novamente' })
                return
            }

            if (result.length > 0) {
                var email = req.body.email
                if (await bcrypt.compare(req.body.password, result[0].pass)) {
                    // Create token
                    const token = jwt.sign(
                      { user_id: result[0].id, email },
                          process.env.TOKEN_KEY,
                      {
                        expiresIn: "2h",
                      }
                    );
              
                    // save user token
                    var user = {
                        nome: result[0].name,
                        auth: true,
                        id: result[0].id,
                        email: result[0].email,
                        lastName: result[0].name_last,
                        token: token
                    }
                    req.session.login = user;
                    model.updateToken([token,user.id], dbConnection, (err, result) => {
                        if(result){
                            res.redirect("/")
                        }else{
                            res.render("admin/form_login", { msg: 'Falha no login, tente novamente 1' })
                        }
                    })

                    
              
                }
            } else {
                res.render("admin/form_login", { msg: 'Falha no login, tente novamente 1' })
            }

        });

    })

    app.get('/sig-out', function (req, res) {
        req.session = null
        res.redirect("/")
    })

    app.get('/profile-update', (req, res) => {
        res.render('admin/form_register_update', { user: req.session.login })
    })

    app.get('/profile-create', (req, res) => {
        res.render('admin/form_register_create');
    })

    app.post('/sig-prfil', async (req, res) => {
        const argon2 = require('argon2');
        var model = app.app.models.user;
        var dbConnection = app.config.dbConnection();
        var post = req.body;

        var data = [
            post.name,
            post.lastname,
        ];

        var id = req.session.login.id;
        if (post.password) {
            data.push(await argon2.hash(post.password));
        }

        data.push(id);

        model.update(data, post, dbConnection, (err, result) => {
            if (result) {
                req.session.login.nome = post.name;
                req.session.login.lastName = post.lastname;
                req.session.login.nome = post.name;
                res.redirect('/')
            }
        })
    })

    app.post('/create-perfil', async (req, res) => {
        // Our register logic starts here
        try {
            // Get user input
            const { name, lastname, email, password } = req.body;

            // Validate user input
            if (!(email && password && name && lastname)) {
                res.status(400).send("All input is required");
            }

            // check if user already exist
            // Validate if user exist in our database
            var model = app.app.models.user;
            var dbConnection = app.config.dbConnection();

            //   const oldUser = await User.findOne({ email });

            //   if (oldUser) {
            //     return res.status(409).send("User Already Exist. Please Login");
            //   }

            //Encrypt user password
            encryptedPassword = await bcrypt.hash(password, 10);

            // Create user in our database
            var data = [
                name,
                lastname,
                email,
                encryptedPassword
            ];

            model.insert(data, dbConnection, async (err, result) => {
                let user = {
                    _id: result.insertId
                }
                // Create token
                const token = jwt.sign(
                    { user_id: user._id, email },
                        process.env.TOKEN_KEY,
                    {
                        expiresIn: "2h",
                    }
                );
                // save user token
                user.token = token;
                // return new user

                model.updateToken([token,user._id], dbConnection, (err, result) => {
                    if (result) {
                        req.session.login = {
                            nome : name,
                            lastName : lastname,
                            token : token,
                            id: user._id
                        }
                        
                        res.redirect('/')
                    }
                })
        
        });

        } catch (err) {
            console.log(err);
        }
        // Our register logic ends here

        // const argon2 = require('argon2');
        // var model = app.app.models.user;
        // var dbConnection = app.config.dbConnection();
        // var post = req.body;
        // var data = [
        //     post.name,
        //     post.lastname,
        //     post.email
        // ];
        // data.push(await argon2.hash(post.password));
        // console.log(data);
        // model.insert(data, dbConnection, (err, result) => {
        //     if(result){
        //         res.redirect('/')
        //     }
        // })

    })

}